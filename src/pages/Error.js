import Error from './../components/Banner'
const error = {
	title: '404 Page Not Found',
	content: 'The page you are looking for does not exist'

}
export default function Courses (){
	return(
		<Error bannerData={error}/>
	)
}