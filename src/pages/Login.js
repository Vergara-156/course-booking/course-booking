import Login from './../components/Banner'
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'
import {Navigate} from 'react-router-dom'
import {Form,Button,Container} from 'react-bootstrap'
import Swal from 'sweetalert2'
const data = {
	title: 'Sign-in Here',
	content: 'Sign in your account below'
}

export default function Signin(){
	const {user, setUser} = useContext(UserContext)
	const [email,setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false)
	const [isValid , setIsValid]= useState(false)

	
	let addressSign = email.search('@')

	let dns = email.search('.com')

	useEffect(() => {
		if (dns !== -1 && addressSign !== -1) {
			setIsValid(true);
			if (password !== '') {
				setIsActive(true)
			} else {
				setIsActive(false)

			}
		} else {
			setIsValid(false);
			setIsActive(false)

			}
		},[email,password,addressSign,dns]);

	const loginUser = (eventSubmit) =>{
		eventSubmit.preventDefault()
		
			fetch(`https://agile-brushlands-62754.herokuapp.com/users/login/`,{
				method: 'POST',
				headers: {
					'Content-Type':'application/json'
				},
				body: JSON.stringify({
					email:email,
					password: password
				})
			}).then(res => res.json()).then(dataJSONderulo =>{
				let token = dataJSONderulo.accessToken
				// console.log(token)

				if (typeof token !== 'undefined') {
					localStorage.setItem('accessToken',token)
			
			fetch('https://agile-brushlands-62754.herokuapp.com/users/details', {
				   headers: {
				      Authorization: `Bearer ${token}`
				   }
				})
				.then(res => res.json())
				.then(convertedData => {
				   
				   if (typeof convertedData._id !== "undefined") {

				     setUser({
				        id: convertedData._id, 
				        isAdmin: convertedData.isAdmin
				     });

				 	 Swal.fire({
						icon: 'success',
						title: 'Login Successful',
						text: 'Welcome!'
					 })	

				 
				   } else {
				      //if the condition above is not met
				      setUser({
				        id: null, 
				        isAdmin: null
				     });
				   }
				});
	

				} else {
				Swal.fire({
				icon: 'error',
				title: 'Login Failed',
				text: "Check your credentials. Contact admin if still persist"
			})		
				}
			})
			
			
			
	};
	return(
		user.id ?
			<Navigate to='/courses' replace = {true}/>
		:
	<>
		<Login bannerData = {data}/>
		<Container>
		<h1 className="text-center">Login Form</h1>
		<Form onSubmit={e => loginUser(e)}>
			<Form.Group>
				<Form.Label>Email:</Form.Label>
				<Form.Control type="email" placeholder="Insert Email" required value={email} onChange={event => {setEmail(event.target.value)}}/>
			{
			isValid ?
			<h6 className='text-success'>Email is Valid</h6>
			:
			<h6 className='text-mute'>Email is Invalid</h6>
		}
			</Form.Group>
			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Insert Password" required value={password}onChange={e => {setPassword(e.target.value)}}/>
			</Form.Group>
			{isActive ?
			<Button className='btn-block' variant= 'success' type='submit'>Login</Button>
			:
			<Button className='btn-block' variant= 'secondary' disabled >Login</Button>

			}
		</Form>
		</Container>
	</>
	)
}