import Login from './../components/Banner'
import {Form,Button,Container} from 'react-bootstrap'
import Swal from 'sweetalert2'
const data = {
	title: 'Add your courses here',
	content: 'Add your courses below'
}

export default function AddCourse(){
	const loginUser = (eventSubmit) =>{
		eventSubmit.preventDefault()
		return(
			
			Swal.fire({
				icon: 'Success',
				title: 'You have added a course',
				text: "Success!"
			})	
			
			);
	};
	return(
	<>
		<Login bannerData = {data}/>
		<Container>
		<h1 className="text-center">Course</h1>
		<Form onSubmit={e => loginUser(e)}>
			<Form.Group>
				<Form.Label>Course Name:</Form.Label>
				<Form.Control type="text" placeholder="Enter Course Name Here" required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Course Description:</Form.Label>
				<Form.Control type="text" placeholder="Enter Description Here" required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Price:</Form.Label>
				<Form.Control type="text" placeholder="Enter Price" required/>
			</Form.Group>
			<Button className='btn-block' variant= 'success' type='submit'>Submit</Button>
		</Form>
		</Container>
	</>
	)
}